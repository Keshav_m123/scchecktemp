//
//  ViewController.swift
//  SampleSC
//
//  Created by Keshav MB on 29/04/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.subBranch()
    }

    func subBranch() {
        print("SubBranch is working")
    }

}

